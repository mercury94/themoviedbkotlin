package com.mercuriy94.themoviedb.presentation.module.movies.di

import com.mercuriy94.themoviedb.presentation.common.scope.ActivityScope
import com.mercuriy94.themoviedb.presentation.module.movies.view.MoviesViewActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by nikit on 24.11.2017.
 */
@ActivityScope
@Subcomponent(modules = arrayOf(MoviesModule::class))
interface MoviesSubcomponent : AndroidInjector<MoviesViewActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MoviesViewActivity>() {}

}