package com.mercuriy94.themoviedb.presentation.model

/**
 * Created by nikit on 25.11.2017.
 */
data class MovieView(val id: String,
                     val title: String,
                     val overview: String,
                     val previewPoster: String?,
                     val originalPoster: String?)