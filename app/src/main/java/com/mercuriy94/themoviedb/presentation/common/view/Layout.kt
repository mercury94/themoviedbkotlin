package com.mercuriy94.themoviedb.presentation.common.view

import android.support.annotation.LayoutRes

/**
 * Created by nikit on 19.11.2017.
 */
@Retention(AnnotationRetention.RUNTIME)
annotation class Layout(@LayoutRes val value: Int = 0)