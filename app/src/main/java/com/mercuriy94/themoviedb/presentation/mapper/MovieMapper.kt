package com.mercuriy94.themoviedb.presentation.mapper

import android.arch.core.util.Function
import com.mercuriy94.themoviedb.data.mapper.Mapper
import com.mercuriy94.themoviedb.domain.entity.Movie
import com.mercuriy94.themoviedb.presentation.model.MovieView

/**
 * Created by nikit on 25.11.2017.
 */
class MovieMapper() : Mapper<Movie, MovieView>() {

    override fun transform(): Function<Movie, MovieView> {
        return Function { i ->
            MovieView(i.id,
                    i.title,
                    i.overview,
                    i.previewPoster,
                    i.originalPoster)
        }
    }

}