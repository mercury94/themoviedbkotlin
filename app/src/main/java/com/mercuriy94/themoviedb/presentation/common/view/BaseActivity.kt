package com.mercuriy94.themoviedb.presentation.common.view

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast


/**
 * Created by nikit on 18.11.2017.
 */
abstract class BaseActivity : MvpAppCompatActivity(), IBaseView {


    override fun onCreate(savedInstanceState: Bundle?) {
        resolveDependencies()
        super.onCreate(savedInstanceState)
        bindLayout()
    }

    protected open fun initViews() {
        //do override
    }

    override fun showPending(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hidePending() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showLongToast(message: String) = longToast(message)

    override fun showShortToast(message: String) = toast(message)


    private fun bindLayout() {
        val cls = this::class
        val layout = cls.annotations.find { it.annotationClass == Layout::class } as? Layout
        layout?.value?.let { it ->
            setContentView(it)
            initViews()
        }
    }

    protected open fun resolveDependencies() {
        //do override
    }
}