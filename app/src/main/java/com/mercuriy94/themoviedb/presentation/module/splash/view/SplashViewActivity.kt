package com.mercuriy94.themoviedb.presentation.module.splash.view

import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.mercuriy94.themoviedb.R
import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity
import com.mercuriy94.themoviedb.presentation.common.view.Layout
import com.mercuriy94.themoviedb.presentation.module.movies.view.MoviesViewActivity
import com.mercuriy94.themoviedb.presentation.module.splash.AbstractSplashPresenter
import com.mercuriy94.themoviedb.presentation.module.splash.ISplashView
import com.mercuriy94.themoviedb.presentation.module.splash.SplashModuleContract.MOVIES_SCREEN_KEY
import dagger.android.AndroidInjection
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace
import javax.inject.Inject

/**
 * Created by nikit on 18.11.2017.
 */
@Layout(R.layout.act_splash_layout)
class SplashViewActivity : BaseActivity(), ISplashView {

    @Inject
    @InjectPresenter
    lateinit var presenter: AbstractSplashPresenter

    @Inject
    lateinit var mNavHolder: NavigatorHolder

    @ProvidePresenter
    fun providePresenter(): AbstractSplashPresenter = presenter

    override fun resolveDependencies() = AndroidInjection.inject(this)

    override fun onResume() {
        super.onResume()
        mNavHolder.setNavigator(SplashNavigator())
    }

    override fun onPause() {
        super.onPause()
        mNavHolder.removeNavigator()
    }

   private inner class SplashNavigator : Navigator {

        override fun applyCommand(command: Command?) {
            if (command is BackTo) backTo(command.screenKey)
            else if (command is Replace) replaceScreen(command.screenKey)
        }

        fun replaceScreen(screenKey: String) {
            when (screenKey) {
                MOVIES_SCREEN_KEY -> startActivity(MoviesViewActivity.newIntent(this@SplashViewActivity))
            }
        }

        fun backTo(screenKey: String?) {
            when (screenKey) {
                null -> this@SplashViewActivity.onBackPressed()
            }
        }
    }

}