package com.mercuriy94.themoviedb.presentation.module.splash.di

import com.mercuriy94.themoviedb.presentation.module.splash.AbstractSplashPresenter
import com.mercuriy94.themoviedb.presentation.module.splash.presenter.SplashPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

/**
 * Created by nikit on 19.11.2017.
 */
@Module
class SplashModule {

    @Provides
    fun providePresenter(router: Router): AbstractSplashPresenter {
        return SplashPresenter(router)
    }

}