package com.mercuriy94.themoviedb.presentation.module.movies.presenter

import com.arellomobile.mvp.InjectViewState
import com.mercuriy94.themoviedb.data.mapper.Mapper
import com.mercuriy94.themoviedb.domain.entity.Movie
import com.mercuriy94.themoviedb.domain.movie.FetchMoviesInteractor
import com.mercuriy94.themoviedb.presentation.model.MovieView
import com.mercuriy94.themoviedb.presentation.module.movies.AbstractMoviesPresenter
import com.mercuriy94.themoviedb.presentation.module.movies.SortConstants
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit

/**
 * Created by nikit on 20.11.2017.
 */
@InjectViewState
class MoviesPresenter(router: Router,
                      private val fetchMoviesInteractor: FetchMoviesInteractor,
                      private val movieMapper: Mapper<Movie, MovieView>) :
        AbstractMoviesPresenter(router) {

    private val SCROLL_THRESHOLD = 7
    private val compositeDisposable = CompositeDisposable()
    private var disposableOnFetchMovies: Disposable? = null
    private var currentSorting = SortConstants.SORT_BY_POPULAR
    private val movies: MutableList<MovieView> = ArrayList()
    private var isTresholdScrollListening: Boolean = false

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        fetchMovies()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!compositeDisposable.isDisposed) compositeDisposable.dispose()
    }

    private fun fetchMovies(userScrolled: Boolean = false) {
        if (currentSorting == SortConstants.SORT_BY_POPULAR) {
            subscribeOnFetchMovies(fetchMoviesInteractor.fetchPopularMovies(),
                    userScrolled = userScrolled)
        } else if (currentSorting == SortConstants.SORT_BY_TOP_RATED) {
            subscribeOnFetchMovies(fetchMoviesInteractor.fetchTopRatedMovies(),
                    userScrolled = userScrolled)
        }
    }

    override fun onMoviesScrolled(lastVisiblePosition: Int) {
        if (isTresholdScrollListening && isBreakTreshold(lastVisiblePosition)) {
            fetchMovies(userScrolled = true)
        }
    }

    private fun isBreakTreshold(lastVisibleMoviesPosition: Int): Boolean {
        return movies.size - lastVisibleMoviesPosition <= SCROLL_THRESHOLD
    }

    override fun onClickMenuSort() = viewState.showSortPopupMenu(currentSorting)

    override fun onSelectedSortMenu(sort: Int) {
        if (sort != currentSorting) {
            if (sort == SortConstants.SORT_BY_POPULAR) {
                currentSorting = sort
                viewState.clearData()
                subscribeOnFetchMovies(fetchMoviesInteractor.refreshPopularMoviesPages())
            } else if (sort == SortConstants.SORT_BY_TOP_RATED) {
                currentSorting = sort
                viewState.clearData()
                subscribeOnFetchMovies(fetchMoviesInteractor.refreshTopRatedMoviesPages())
            }
        }
    }

    override fun onRefreshMovies() {
        if (currentSorting == SortConstants.SORT_BY_POPULAR) {
            subscribeOnFetchMovies(fetchMoviesInteractor.refreshPopularMoviesPages(),
                    userRefreshing = true)
        } else if (currentSorting == SortConstants.SORT_BY_TOP_RATED) {
            subscribeOnFetchMovies(fetchMoviesInteractor.refreshTopRatedMoviesPages(),
                    userRefreshing = true)
        }
    }

    private fun subscribeOnFetchMovies(fetchMoviesObs: Observable<List<Movie>>,
                                       userRefreshing: Boolean = false,
                                       userScrolled: Boolean = false) {

        disposableOnFetchMovies?.let { i ->
            if (!i.isDisposed) i.dispose()
        }

        disposableOnFetchMovies = fetchMoviesObs.map { t -> movieMapper.execute(t) }
                .delay(3, TimeUnit.SECONDS, Schedulers.computation())
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe({
                    isTresholdScrollListening = false
                    when {
                        userScrolled -> viewState.showPending(footerPending = true)
                        userRefreshing -> viewState.showPending(userRefreshing = userRefreshing)
                        else -> viewState.showPending()
                    }
                })
                .doFinally({ isTresholdScrollListening = true })
                .subscribe({ t ->
                    if (userRefreshing || !userScrolled) {
                        movies.clear()
                        viewState.clearData()
                    }
                    movies.addAll(t)
                    viewState.showMovies(ArrayList(movies))
                    viewState.hidePending()
                })

        disposableOnFetchMovies?.let { i ->
            compositeDisposable.add(i)
        }
    }

}