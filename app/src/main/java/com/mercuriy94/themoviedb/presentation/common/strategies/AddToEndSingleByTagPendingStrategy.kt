package com.mercuriy94.themoviedb.presentation.common.strategies

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.ViewCommand
import com.arellomobile.mvp.viewstate.strategy.StateStrategy

/**
 * Created by nikit on 26.11.2017.
 */
class AddToEndSingleByTagPendingStrategy : StateStrategy {

    companion object {

        const val TAG_SHOW_PENDING = "show_pending"
        const val TAG_HIDE_PENDING = "hide_pending"

    }

    override fun <View : MvpView> beforeApply(currentState: MutableList<ViewCommand<View>>,
                                              incomingCommand: ViewCommand<View>) {

        if (incomingCommand.tag == TAG_SHOW_PENDING ||
                incomingCommand.tag.contains(TAG_HIDE_PENDING)) {
            removePendingViewCommand(currentState)
            currentState.add(incomingCommand)
        }

    }

    override fun <View : MvpView> afterApply(currentState: List<ViewCommand<View>>,
                                             incomingCommand: ViewCommand<View>) {
        //do nothing
    }

    private fun <View : MvpView> removePendingViewCommand(
            currentState: MutableList<ViewCommand<View>>) {
        val iterator = currentState.iterator()
        while (iterator.hasNext()) {
            val viewCommand = iterator.next()
            if (viewCommand.tag != null && viewCommand.tag == TAG_SHOW_PENDING ||
                    viewCommand.tag == TAG_HIDE_PENDING) {
                iterator.remove()
                break
            }
        }
    }
}
