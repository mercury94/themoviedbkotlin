package com.mercuriy94.themoviedb.presentation.common.presenter

import com.arellomobile.mvp.MvpPresenter
import com.mercuriy94.themoviedb.presentation.common.view.IBaseView
import ru.terrakok.cicerone.Router

/**
 * Created by nikit on 19.11.2017.
 */
abstract class BasePresenter<View : IBaseView>(var router: Router) : MvpPresenter<View>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

    }

}