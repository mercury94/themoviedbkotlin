package com.mercuriy94.themoviedb.presentation.module.movies.view

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Typeface
import android.support.design.widget.AppBarLayout
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.mercuriy94.themoviedb.R
import com.mercuriy94.themoviedb.presentation.common.view.BaseActivity
import com.mercuriy94.themoviedb.presentation.common.view.Layout
import com.mercuriy94.themoviedb.presentation.model.MovieView
import com.mercuriy94.themoviedb.presentation.module.movies.AbstractMoviesPresenter
import com.mercuriy94.themoviedb.presentation.module.movies.IMoviesView
import com.mercuriy94.themoviedb.presentation.module.movies.SortConstants
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.act_movies_layout.*
import kotlinx.android.synthetic.main.list_helper_layout.*
import javax.inject.Inject

/**
 * Created by nikit on 20.11.2017.
 */
@Layout(R.layout.act_movies_layout)
class MoviesViewActivity : BaseActivity(), IMoviesView, PopupMenu.OnMenuItemClickListener {

    @Inject
    @InjectPresenter
    lateinit var presenter: AbstractMoviesPresenter
    private var mSpanCount = 0
    private var layoutManager: StaggeredGridLayoutManager? = null
    private var moviesRecyclerAdapter: MoviesRecyclerAdapter? = null

    companion object {

        fun newIntent(context: Context): Intent = Intent(context, MoviesViewActivity::class.java)

    }

    @ProvidePresenter
    fun providePresenter(): AbstractMoviesPresenter = presenter

    override fun resolveDependencies() = AndroidInjection.inject(this)

    override fun initViews() {
        setSupportActionBar(toolbar)
        initList()
        initRefreshLayout()
    }

    private fun initList() {
        moviesRecyclerAdapter = MoviesRecyclerAdapter()
        moviesRecyclerView.adapter = moviesRecyclerAdapter
        mSpanCount = when (resources.configuration.orientation) {
            Configuration.ORIENTATION_PORTRAIT -> 2
            else -> 3
        }
        layoutManager = StaggeredGridLayoutManager(mSpanCount, StaggeredGridLayoutManager.VERTICAL)
        moviesRecyclerView.layoutManager = layoutManager
        moviesRecyclerView.addOnScrollListener(MoviesRecyclerScrollListener())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.movies_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_sort -> {
                presenter.onClickMenuSort()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener({ presenter.onRefreshMovies() })
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary)
    }

    private fun enableScrollBehavior() {
        val layoutParams = collapsingToolbar.layoutParams
        if (layoutParams is AppBarLayout.LayoutParams) {
            layoutParams.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or
                    AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP or
                    AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
            collapsingToolbar.layoutParams = layoutParams
        }
    }

    private fun disableScrollBehavior() {
        val layoutParams = collapsingToolbar.layoutParams
        if (layoutParams is AppBarLayout.LayoutParams) {
            layoutParams.scrollFlags = 0
            collapsingToolbar.layoutParams = layoutParams
        }
    }

    override fun showSortPopupMenu(currentSort: Int) {
        val menuItemView = findViewById<View>(R.id.action_sort)
        val popupMenuSort = PopupMenu(this, menuItemView)
        popupMenuSort.inflate(R.menu.movies_sort_menu)
        val menuItem = popupMenuSort.menu.getItem(currentSort)
        val s = SpannableString(menuItem.title)
        val boldSpan = StyleSpan(Typeface.BOLD)
        s.setSpan(boldSpan, 0, menuItem.title.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        menuItem.title = s
        popupMenuSort.setOnMenuItemClickListener(this)
        popupMenuSort.show()
    }

    override fun showPending(message: String?, userRefreshing: Boolean, footerPending: Boolean) {
        when {
            userRefreshing -> swipeRefreshLayout.isRefreshing = true
            footerPending -> {
                moviesRecyclerAdapter?.isVisiblePendingFooter = true
                swipeRefreshLayout.isEnabled = false
            }
            else -> showPending()
        }
    }

    override fun showPending(message: String?) {
        progressBarView.visibility = View.VISIBLE
        swipeRefreshLayout.isEnabled = false
        disableScrollBehavior()
    }

    override fun hidePending() {
        swipeRefreshLayout.isEnabled = true
        progressBarView.visibility = View.GONE
        moviesRecyclerAdapter?.isVisiblePendingFooter = false
        swipeRefreshLayout.isRefreshing = false
        enableScrollBehavior()
    }

    override fun clearData() {
        moviesRecyclerAdapter?.removeAllItems()
        moviesRecyclerView.scrollToPosition(0)
    }

    override fun showMovies(movies: List<MovieView>) {
        moviesRecyclerAdapter?.setList(movies)
    }

    private inner class MoviesRecyclerScrollListener : RecyclerView.OnScrollListener() {

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            val lastVisibleMoviesPositions = layoutManager?.
                    findLastVisibleItemPositions(IntArray(mSpanCount))
            lastVisibleMoviesPositions?.get(mSpanCount - 1)?.let { presenter.onMoviesScrolled(it) }
        }
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.movies_sort_by_popular -> {
                presenter.onSelectedSortMenu(SortConstants.SORT_BY_POPULAR)
                true
            }
            R.id.movies_sort_by_top_rated -> {
                presenter.onSelectedSortMenu(SortConstants.SORT_BY_TOP_RATED)
                true
            }
            else -> false
        }
    }
}