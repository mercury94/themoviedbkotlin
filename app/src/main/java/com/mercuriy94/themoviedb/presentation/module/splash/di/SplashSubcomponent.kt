package com.mercuriy94.themoviedb.presentation.module.splash.di

import com.mercuriy94.themoviedb.presentation.common.scope.ActivityScope
import com.mercuriy94.themoviedb.presentation.module.splash.view.SplashViewActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by nikit on 19.11.2017.
 */
@ActivityScope
@Subcomponent(modules = arrayOf(SplashModule::class))
interface SplashSubcomponent : AndroidInjector<SplashViewActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<SplashViewActivity>() {}

}