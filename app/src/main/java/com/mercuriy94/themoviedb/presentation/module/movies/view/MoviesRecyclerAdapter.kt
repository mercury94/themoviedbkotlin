package com.mercuriy94.themoviedb.presentation.module.movies.view

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mercuriy94.themoviedb.R
import com.mercuriy94.themoviedb.presentation.model.MovieView
import com.mercuriy94.themoviedb.presentation.utils.GlideApp
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.act_movies_recycler_li_layout.view.*
import kotlinx.android.synthetic.main.list_helper_layout.view.*

/**
 * Created by nikit on 22.11.2017.
 */
class MoviesRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_ITEM = 0
    private val TYPE_FOOTER = 1
    private val countFooters = 1

    var isVisiblePendingFooter = false
        set(value) {
            field = value
            if (field) isVisibleErrorLoadDataFooter = false
            notifyItemChanged(itemCount - 1)
        }

    var isVisibleErrorLoadDataFooter = false
        set(value) {
            field = value
            if (field) isVisiblePendingFooter = false
            notifyItemChanged(itemCount - 1)
        }

    private var listMovies: MutableList<MovieView> = ArrayList()

    fun setList(movies: List<MovieView>) {
        if (listMovies.isEmpty()) {
            listMovies.addAll(movies)
            notifyDataSetChanged()
        } else {
            Observable.fromCallable({
                return@fromCallable DiffUtil.calculateDiff(DiffMoviesCallback(listMovies,
                        movies, countFooters))
            }).subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t ->
                        listMovies.clear()
                        listMovies.addAll(movies)
                        t.dispatchUpdatesTo(this)
                    })
        }
    }

    fun removeAllItems() {
        listMovies.clear()
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        holder?.let { i ->
            (i as? ViewHolderItem)?.bind(listMovies[position]) ?: (i as? ViewHolderFooter)?.bind()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isFooter(position)) TYPE_FOOTER else TYPE_ITEM
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> ViewHolderItem(LayoutInflater.from(parent?.let { parent.context })
                    .inflate(R.layout.act_movies_recycler_li_layout, parent, false))
            else -> ViewHolderFooter(LayoutInflater.from(parent?.let { parent.context })
                    .inflate(R.layout.list_helper_layout, parent, false))
        }
    }

    override fun getItemCount(): Int = listMovies.size + countFooters

    private fun isFooter(position: Int): Boolean = position > listMovies.size - 1

    private inner class ViewHolderItem(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        fun bind(movieView: MovieView) {
            itemView.movieTvTitle.text = movieView.title
            GlideApp.with(itemView.context)
                    .load(movieView.previewPoster)
                    .placeholder(R.drawable.image_paceholder)
                    .into(itemView.movieItemIvPreview)
        }
    }

    private inner class ViewHolderFooter(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val visible = when {
                isVisiblePendingFooter -> {
                    itemView.progressBarView.visibility = View.VISIBLE
                    itemView.errorLoadDataView.visibility = View.GONE
                    true
                }
                isVisibleErrorLoadDataFooter -> {
                    itemView.progressBarView.visibility = View.GONE
                    itemView.errorLoadDataView.visibility = View.VISIBLE
                    true
                }
                else -> false
            }

            val lp = StaggeredGridLayoutManager.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    if (visible) ViewGroup.LayoutParams.WRAP_CONTENT else 0)
            lp.isFullSpan = true
            itemView.layoutParams = lp

        }
    }
}