package com.mercuriy94.themoviedb.presentation.module.splash.presenter

import com.arellomobile.mvp.InjectViewState
import com.mercuriy94.themoviedb.presentation.module.splash.AbstractSplashPresenter
import com.mercuriy94.themoviedb.presentation.module.splash.SplashModuleContract.MOVIES_SCREEN_KEY
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit

/**
 * Created by nikit on 19.11.2017.
 */

@InjectViewState
class SplashPresenter(router: Router) : AbstractSplashPresenter(router) {

    private val TIME_OUT = 1

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        Observable.timer(TIME_OUT.toLong(), TimeUnit.SECONDS, Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ router.newRootScreen(MOVIES_SCREEN_KEY) })
    }

}