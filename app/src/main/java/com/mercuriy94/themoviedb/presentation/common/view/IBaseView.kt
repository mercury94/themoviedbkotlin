package com.mercuriy94.themoviedb.presentation.common.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.mercuriy94.themoviedb.presentation.common.strategies.AddToEndSingleByTagPendingStrategy

/**
 * Created by nikit on 18.11.2017.
 */
interface IBaseView : MvpView {

    @StateStrategyType(value = AddToEndSingleByTagPendingStrategy::class,
            tag = AddToEndSingleByTagPendingStrategy.TAG_SHOW_PENDING)
    fun showPending(message: String? = null)

    @StateStrategyType(value = AddToEndSingleByTagPendingStrategy::class,
            tag = AddToEndSingleByTagPendingStrategy.TAG_HIDE_PENDING)
    fun hidePending()

    @StateStrategyType(value = SkipStrategy::class)
    fun showLongToast(message: String)

    @StateStrategyType(value = SkipStrategy::class)
    fun showShortToast(message: String)

}