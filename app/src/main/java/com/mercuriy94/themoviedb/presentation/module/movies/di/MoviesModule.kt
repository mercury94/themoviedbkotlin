package com.mercuriy94.themoviedb.presentation.module.movies.di

import com.mercuriy94.themoviedb.data.mapper.Mapper
import com.mercuriy94.themoviedb.domain.entity.Movie
import com.mercuriy94.themoviedb.domain.movie.FetchMoviesInteractor
import com.mercuriy94.themoviedb.domain.movie.IMoviesRepository
import com.mercuriy94.themoviedb.presentation.mapper.MovieMapper
import com.mercuriy94.themoviedb.presentation.model.MovieView
import com.mercuriy94.themoviedb.presentation.module.movies.AbstractMoviesPresenter
import com.mercuriy94.themoviedb.presentation.module.movies.presenter.MoviesPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

/**
 * Created by nikit on 24.11.2017.
 */
@Module
class MoviesModule {

    @Provides
    fun provideFetchPopularMoviesInteractor(repository: IMoviesRepository): FetchMoviesInteractor {
        return FetchMoviesInteractor(repository)
    }

    @Provides
    fun provideModelViewMapper(): Mapper<Movie, MovieView> = MovieMapper()

    @Provides
    fun providePresenter(router: Router,
                         fetchPopularMoviesInteractor: FetchMoviesInteractor,
                         movieMapper: Mapper<Movie, MovieView>): AbstractMoviesPresenter {
        return MoviesPresenter(router, fetchPopularMoviesInteractor, movieMapper)
    }


}