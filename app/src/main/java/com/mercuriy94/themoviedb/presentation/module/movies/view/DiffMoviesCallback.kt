package com.mercuriy94.themoviedb.presentation.module.movies.view

import android.support.v7.util.DiffUtil
import com.mercuriy94.themoviedb.presentation.model.MovieView

/**
 * Created by nikit on 27.11.2017.
 */

class DiffMoviesCallback(private val oldMovieList: List<MovieView>,
                         private val newMovieList: List<MovieView>,
                         private val countFooters: Int) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldMovieList.size + countFooters

    override fun getNewListSize(): Int = newMovieList.size + countFooters

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return if (isFooter(oldItemPosition, newItemPosition)) false
        else oldMovieList[oldItemPosition].id == newMovieList[newItemPosition].id
    }

    private fun isFooter(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItemPosition >= oldMovieList.size || newItemPosition >= newMovieList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldMovieList[oldItemPosition].title == newMovieList[newItemPosition].title
    }
}