package com.mercuriy94.themoviedb.presentation.common.scope

import javax.inject.Scope

/**
 * Created by nikit on 20.11.2017.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope()
