package com.mercuriy94.themoviedb.presentation.module.splash

import com.mercuriy94.themoviedb.presentation.common.presenter.BasePresenter
import com.mercuriy94.themoviedb.presentation.common.view.IBaseView
import ru.terrakok.cicerone.Router

/**
 * Created by nikit on 18.11.2017.
 */

object SplashModuleContract {
     val MOVIES_SCREEN_KEY = "movies_screen"
}

interface ISplashView : IBaseView {

}

abstract class AbstractSplashPresenter(router: Router) : BasePresenter<ISplashView>(router) {


}