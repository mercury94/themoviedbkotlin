package com.mercuriy94.themoviedb.presentation.module.movies

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.mercuriy94.themoviedb.presentation.common.presenter.BasePresenter
import com.mercuriy94.themoviedb.presentation.common.strategies.AddToEndSingleByTagPendingStrategy
import com.mercuriy94.themoviedb.presentation.common.view.IBaseView
import com.mercuriy94.themoviedb.presentation.model.MovieView
import com.mercuriy94.themoviedb.presentation.module.movies.strategies.MoviesShowDataStrategy
import ru.terrakok.cicerone.Router

/**
 * Created by nikit on 19.11.2017.
 */

object SortConstants {

    const val SORT_BY_POPULAR = 0
    const val SORT_BY_TOP_RATED = 1

}

interface IMoviesView : IBaseView {

    @StateStrategyType(value = MoviesShowDataStrategy::class, tag = MoviesShowDataStrategy.TAG_SHOW_DATA)
    fun showMovies(movies: List<MovieView>)

    @StateStrategyType(value = MoviesShowDataStrategy::class, tag = MoviesShowDataStrategy.TAG_HIDE_DATA)
    fun clearData()

    @StateStrategyType(value = SkipStrategy::class)
    fun showSortPopupMenu(currentSort: Int)

    @StateStrategyType(value = AddToEndSingleByTagPendingStrategy::class,
            tag = AddToEndSingleByTagPendingStrategy.TAG_SHOW_PENDING)
    fun showPending(message: String? = null,
                    userRefreshing: Boolean = false,
                    footerPending: Boolean = false)
}

abstract class AbstractMoviesPresenter(router: Router) : BasePresenter<IMoviesView>(router) {

    abstract fun onMoviesScrolled(lastVisiblePosition: Int)

    abstract fun onRefreshMovies()

    abstract fun onClickMenuSort()

    abstract fun onSelectedSortMenu(sort: Int)

}