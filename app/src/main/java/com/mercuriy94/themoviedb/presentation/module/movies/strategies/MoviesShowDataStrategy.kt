package com.mercuriy94.themoviedb.presentation.module.movies.strategies

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.ViewCommand
import com.arellomobile.mvp.viewstate.strategy.StateStrategy

/**
 * Created by nikit on 26.11.2017.
 */
class MoviesShowDataStrategy : StateStrategy {

    companion object {

        const val TAG_SHOW_DATA = "show_data"
        const val TAG_HIDE_DATA = "hide_data"

    }

    override fun <View : MvpView> beforeApply(currentState: MutableList<ViewCommand<View>>,
                                              incomingCommand: ViewCommand<View>) {

        if (incomingCommand.tag == TAG_SHOW_DATA || incomingCommand.tag.contains(TAG_HIDE_DATA)) {
            removePendingViewCommand(currentState)
            currentState.add(incomingCommand)
        }

    }

    override fun <View : MvpView> afterApply(currentState: List<ViewCommand<View>>,
                                             incomingCommand: ViewCommand<View>) {
        //do nothing
    }

    private fun <View : MvpView> removePendingViewCommand(
            currentState: MutableList<ViewCommand<View>>) {
        val iterator = currentState.iterator()
        while (iterator.hasNext()) {
            val viewCommand = iterator.next()
            if (viewCommand.tag != null && viewCommand.tag == TAG_SHOW_DATA ||
                    viewCommand.tag == TAG_HIDE_DATA) {
                iterator.remove()
                break
            }
        }
    }
}