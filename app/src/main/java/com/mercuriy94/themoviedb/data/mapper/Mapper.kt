package com.mercuriy94.themoviedb.data.mapper

import android.arch.core.util.Function


/**
 * Created by nikit on 25.11.2017.
 */
abstract class Mapper<Source, Destination> {

    protected abstract fun transform(): Function<Source, Destination>

    fun execute(source: Source): Destination = transform().apply(source)

    fun execute(sources: List<Source>): List<Destination> = sources.map { s -> transform().apply(s) }

}