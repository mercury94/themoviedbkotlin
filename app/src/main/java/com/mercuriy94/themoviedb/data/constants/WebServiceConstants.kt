package com.mercuriy94.themoviedb.data.constants

/**
 * Created by nikit on 24.11.2017.
 */
object WebServiceConstants {


    //region common

    const val API_KEY = "4710d4a540dfda7c6ccc8ece01a89e7e"

    //region common

    //region urls

    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val BASE_IMAGE_URL = "http://image.tmdb.org/t/p/"

    const val MOVIE_URL = BASE_URL + "movie"
    const val POPULAR_MOVIES_URL = MOVIE_URL + "/popular"
    const val TOP_RATED_MOVIES_URL = MOVIE_URL + "/top_rated"

    const val IMAGE_PREVIEW_URL = BASE_IMAGE_URL + "w342"
    const val IMAGE_POSTER_URL = BASE_IMAGE_URL + "w780"

    //endregion urls
}