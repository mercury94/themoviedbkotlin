package com.mercuriy94.themoviedb.data.repository

import com.mercuriy94.themoviedb.data.repository.web.IMoviesWebRepository
import com.mercuriy94.themoviedb.domain.entity.Movie
import com.mercuriy94.themoviedb.domain.movie.IMoviesRepository
import io.reactivex.Observable

/**
 * Created by nikit on 18.11.2017.
 */
class MoviesRepository(val repository: IMoviesWebRepository) : IMoviesRepository {

    override fun fetchPopularMovies(page: Int): Observable<List<Movie>> {
        return repository.fetchPopularMovies(page)
    }

    override fun fetchTopRatedMovies(page: Int): Observable<List<Movie>> {
        return repository.fetchTopRatedMovies(page)
    }
}