package com.mercuriy94.themoviedb.data.rest.response

import com.google.gson.annotations.SerializedName
import com.mercuriy94.themoviedb.data.entity.dto.MovieDto

/**
 * Created by nikit on 25.11.2017.
 */
data class FetchMoviesResponse(@SerializedName("page") val page: Int,
                               @SerializedName("total_pages") val totalPages: Int,
                               @SerializedName("results") val results: List<MovieDto>,
                               @SerializedName("total_results") val totalResult: Int)