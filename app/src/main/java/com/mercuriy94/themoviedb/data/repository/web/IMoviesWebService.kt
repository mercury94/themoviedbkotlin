package com.mercuriy94.themoviedb.data.repository.web

import com.mercuriy94.themoviedb.data.constants.WebServiceConstants
import com.mercuriy94.themoviedb.data.rest.response.FetchMoviesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by nikit on 24.11.2017.
 */
interface IMoviesWebService {

    @GET(WebServiceConstants.POPULAR_MOVIES_URL)
    fun fetchPopularMovies(@Query("api_key") apiKey: String,
                           @Query("language") language: String,
                           @Query("page") page: Int): Observable<FetchMoviesResponse>

    @GET(WebServiceConstants.TOP_RATED_MOVIES_URL)
    fun fetchTopRatedMovies(@Query("api_key") apiKey: String,
                            @Query("language") language: String,
                            @Query("page") page: Int): Observable<FetchMoviesResponse>

}