package com.mercuriy94.themoviedb.data.repository.web

import com.mercuriy94.themoviedb.data.constants.WebServiceConstants
import com.mercuriy94.themoviedb.data.mapper.MovieDtoMapper
import com.mercuriy94.themoviedb.domain.entity.Movie
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by nikit on 18.11.2017.
 */

class MoviesWebRepository @Inject constructor(private var moviesWebService: IMoviesWebService,
                                              @Named("system_language") private var language: String,
                                              private var movieMapper: MovieDtoMapper) : IMoviesWebRepository {


    override fun fetchPopularMovies(page: Int): Observable<List<Movie>> {
        return moviesWebService.fetchPopularMovies(WebServiceConstants.API_KEY, language, page)
                .map { t -> t.results }
                .map(movieMapper::execute)
    }

    override fun fetchTopRatedMovies(page: Int): Observable<List<Movie>> {
        return moviesWebService.fetchTopRatedMovies(WebServiceConstants.API_KEY, language, page)
                .map { t -> t.results }
                .map(movieMapper::execute)
    }
}