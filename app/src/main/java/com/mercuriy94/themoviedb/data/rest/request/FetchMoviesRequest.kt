package com.mercuriy94.themoviedb.data.rest.request

import com.google.gson.annotations.SerializedName

/**
 * Created by nikit on 24.11.2017.
 */
data class FetchMoviesRequest(@SerializedName("page") val page: Int,
                              @SerializedName("region") var region: String?) : BaseRequest()