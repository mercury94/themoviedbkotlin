package com.mercuriy94.themoviedb.data.mapper

import android.arch.core.util.Function
import com.mercuriy94.themoviedb.data.constants.WebServiceConstants
import com.mercuriy94.themoviedb.data.entity.dto.MovieDto
import com.mercuriy94.themoviedb.domain.entity.Movie
import javax.inject.Inject

/**
 * Created by nikit on 25.11.2017.
 */
class MovieDtoMapper @Inject constructor() : Mapper<MovieDto, Movie>() {

    override fun transform(): Function<MovieDto, Movie> {
        return Function { input ->
            Movie(input.id,
                    input.title,
                    input.overview,
                    getPreviewPosterPathByPosterPath(input.posterPath),
                    getOriginalPosterPathByPosterPath(input.posterPath))
        }
    }

    private fun getPreviewPosterPathByPosterPath(posterPath: String?): String? {
        return if (posterPath != null) WebServiceConstants.IMAGE_PREVIEW_URL + posterPath else null
    }

    private fun getOriginalPosterPathByPosterPath(posterPath: String?): String? {
        return if (posterPath != null) WebServiceConstants.IMAGE_POSTER_URL + posterPath else null
    }

}