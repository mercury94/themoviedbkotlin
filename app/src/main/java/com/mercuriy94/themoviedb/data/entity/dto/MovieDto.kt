package com.mercuriy94.themoviedb.data.entity.dto

import com.google.gson.annotations.SerializedName

/**
 * Created by nikit on 19.11.2017.
 */
data class MovieDto(@SerializedName("id") val id: String,
                    @SerializedName("title") val title: String,
                    @SerializedName("overview") val overview: String,
                    @SerializedName("poster_path") val posterPath: String)