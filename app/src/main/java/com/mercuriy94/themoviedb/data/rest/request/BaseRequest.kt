package com.mercuriy94.themoviedb.data.rest.request

import com.google.gson.annotations.SerializedName

/**
 * Created by nikit on 24.11.2017.
 */
open class BaseRequest(){

    @SerializedName("api_key")
    lateinit var apiKey: String
    @SerializedName("language")
    lateinit var language: String

}