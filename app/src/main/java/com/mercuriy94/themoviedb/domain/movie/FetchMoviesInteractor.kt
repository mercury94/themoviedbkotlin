package com.mercuriy94.themoviedb.domain.movie

import com.mercuriy94.themoviedb.domain.entity.Movie
import io.reactivex.Observable

/**
 * Created by nikit on 18.11.2017.
 */
class FetchMoviesInteractor(moviesRepository: IMoviesRepository) {

    private val popularMoviesPaginator = PopularMoviesPaginator(moviesRepository)

    private val topRatedMoviesPaginator = TopRatedMoviesPaginator(moviesRepository)

    fun fetchPopularMovies(): Observable<List<Movie>> = popularMoviesPaginator.fetchItems()

    fun fetchTopRatedMovies(): Observable<List<Movie>> = topRatedMoviesPaginator.fetchItems()

    fun refreshPopularMoviesPages() = popularMoviesPaginator.refreshPages()

    fun refreshTopRatedMoviesPages() = topRatedMoviesPaginator.refreshPages()

}