package com.mercuriy94.themoviedb.domain.entity

/**
 * Created by nikit on 18.11.2017.
 */
data class Movie(val id: String,
                 val title: String,
                 val overview: String,
                 val previewPoster: String?,
                 val originalPoster: String?)