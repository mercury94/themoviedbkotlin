package com.mercuriy94.themoviedb.domain.common

import io.reactivex.Observable

/**
 * Created by nikit on 26.11.2017.
 */
abstract class Paginator<T> {

    private val FIRST_PAGE = 1
    private var currentPage = FIRST_PAGE

    fun refreshPages(): Observable<List<T>> {
        currentPage = FIRST_PAGE
        return fetchItems()
    }

    protected abstract fun buildFetchItemsObservable(page: Int): Observable<List<T>>

    fun fetchItems(): Observable<List<T>> {
        return buildFetchItemsObservable(currentPage).doOnComplete { currentPage++ }
    }
}