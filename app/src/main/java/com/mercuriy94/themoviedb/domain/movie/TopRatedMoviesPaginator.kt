package com.mercuriy94.themoviedb.domain.movie

import com.mercuriy94.themoviedb.domain.common.Paginator
import com.mercuriy94.themoviedb.domain.entity.Movie
import io.reactivex.Observable

/**
 * Created by nikit on 26.11.2017.
 */
class TopRatedMoviesPaginator(val repository: IMoviesRepository) : Paginator<Movie>() {

    override fun buildFetchItemsObservable(page: Int): Observable<List<Movie>> {
        return repository.fetchTopRatedMovies(page)
    }

}