package com.mercuriy94.themoviedb.domain.movie

import com.mercuriy94.themoviedb.domain.entity.Movie
import io.reactivex.Observable

/**
 * Created by nikit on 18.11.2017.
 */
interface IMoviesRepository {

    fun fetchPopularMovies(page: Int): Observable<List<Movie>>

    fun fetchTopRatedMovies(page: Int): Observable<List<Movie>>

}