package com.mercuriy94.themoviedb.app.di

import android.app.Activity
import com.mercuriy94.themoviedb.presentation.module.movies.di.MoviesSubcomponent
import com.mercuriy94.themoviedb.presentation.module.movies.view.MoviesViewActivity
import com.mercuriy94.themoviedb.presentation.module.splash.di.SplashSubcomponent
import com.mercuriy94.themoviedb.presentation.module.splash.view.SplashViewActivity
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap


/**
 * Created by nikit on 19.11.2017.
 */
@Module
abstract class ActivityBuilder {

    @Binds
    @IntoMap
    @ActivityKey(SplashViewActivity::class)
    abstract fun bindSplashActivity(builder: SplashSubcomponent.Builder): AndroidInjector.Factory<out Activity>

    @Binds
    @IntoMap
    @ActivityKey(MoviesViewActivity::class)
    abstract fun bindMoviesActivity(builder: MoviesSubcomponent.Builder): AndroidInjector.Factory<out Activity>

}