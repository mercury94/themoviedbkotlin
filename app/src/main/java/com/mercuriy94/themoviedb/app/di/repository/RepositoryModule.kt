package com.mercuriy94.themoviedb.app.di.repository

import com.mercuriy94.themoviedb.data.repository.MoviesRepository
import com.mercuriy94.themoviedb.data.repository.web.IMoviesWebRepository
import com.mercuriy94.themoviedb.domain.movie.IMoviesRepository
import dagger.Module
import dagger.Provides

/**
 * Created by nikit on 24.11.2017.
 */
@Module(includes = arrayOf(WebRepositoryModule::class))
class RepositoryModule {

    @Provides
    fun provideMoviesRepository(moviewsWebRepositoty: IMoviesWebRepository): IMoviesRepository {
        return MoviesRepository(moviewsWebRepositoty)
    }

}