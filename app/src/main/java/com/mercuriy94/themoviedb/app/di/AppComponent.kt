package com.mercuriy94.themoviedb.app.di

import android.app.Application
import android.content.Context
import com.mercuriy94.themoviedb.app.App
import com.mercuriy94.themoviedb.app.di.repository.RepositoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

/**
 * Created by nikit on 19.11.2017.
 */
@Singleton
@Component(modules = arrayOf(AndroidInjectionModule::class,
        AppModule::class,
        ActivityBuilder::class,
        NetworkModule::class,
        RepositoryModule::class))
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun context(): Context

    fun router(): Router

    fun inject(app: App)
}