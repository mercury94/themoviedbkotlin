package com.mercuriy94.themoviedb.app.di

import android.util.Log
import com.google.gson.Gson
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import com.mercuriy94.themoviedb.BuildConfig
import com.mercuriy94.themoviedb.data.constants.WebServiceConstants
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by nikit on 24.11.2017.
 */
@Module
class NetworkModule {

    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, retrofitBuilder: Retrofit.Builder): Retrofit {
        return retrofitBuilder.client(okHttpClient).build()
    }

    @Provides
    fun provideRetrofitBuilder(gson: Gson): Retrofit.Builder {
        return Retrofit.Builder()
                .baseUrl(WebServiceConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }

    @Provides
    fun provideOkHttpClientBuilder(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
                .addInterceptor(LoggingInterceptor.Builder()
                        .loggable(BuildConfig.DEBUG)
                        .setLevel(Level.BODY)
                        .log(Log.INFO)
                        .request("Request")
                        .response("Response")
                        .build())
    }

    @Provides
    fun provideOkHttpClient(bulder: OkHttpClient.Builder): OkHttpClient = bulder.build()

    @Provides
    fun provideGson(): Gson = Gson()

}