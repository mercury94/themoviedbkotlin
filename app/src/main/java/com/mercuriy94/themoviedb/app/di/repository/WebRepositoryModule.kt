package com.mercuriy94.themoviedb.app.di.repository

import com.mercuriy94.themoviedb.app.di.NetworkModule
import com.mercuriy94.themoviedb.data.repository.web.IMoviesWebRepository
import com.mercuriy94.themoviedb.data.repository.web.IMoviesWebService
import com.mercuriy94.themoviedb.data.repository.web.MoviesWebRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by nikit on 24.11.2017.
 */
@Module(includes = arrayOf(NetworkModule::class, WebRepositoryModule.Declarations::class))
class WebRepositoryModule {

    @Provides
    fun provideMoviesWebService(retrofit: Retrofit): IMoviesWebService {
        return retrofit.create(IMoviesWebService::class.java)
    }

    @Module
    interface Declarations {

        @Binds
        fun bindMovieWebRepository(moviesWebRepository: MoviesWebRepository): IMoviesWebRepository

    }

}