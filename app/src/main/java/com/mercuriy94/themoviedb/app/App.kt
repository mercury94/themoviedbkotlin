package com.mercuriy94.themoviedb.app

import android.app.Activity
import android.app.Application
import android.content.Context
import com.mercuriy94.themoviedb.app.di.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject


/**
 * Created by nikit on 18.11.2017.
 */
class App : Application(), HasActivityInjector {

    @Inject
    lateinit var mActivityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent
                .builder()
                .application(this)
                .build()
                .inject(this);
    }

    //region HasActivityInjector

    override fun activityInjector(): DispatchingAndroidInjector<Activity> {
        return mActivityDispatchingAndroidInjector;
    }

    //endregion HasActivityInjector
}