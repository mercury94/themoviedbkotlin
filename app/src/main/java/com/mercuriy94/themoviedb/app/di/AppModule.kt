package com.mercuriy94.themoviedb.app.di

import android.annotation.TargetApi
import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import com.mercuriy94.themoviedb.presentation.module.movies.di.MoviesSubcomponent
import com.mercuriy94.themoviedb.presentation.module.splash.di.SplashSubcomponent
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by nikit on 19.11.2017.
 */
@Module(subcomponents = arrayOf(SplashSubcomponent::class, MoviesSubcomponent::class))
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application;

    @Provides
    @Singleton
    fun provideCicerone(): Cicerone<Router> = Cicerone.create()


    @Provides
    @Singleton
    fun provideNavigator(cicerone: Cicerone<Router>): NavigatorHolder = cicerone.navigatorHolder

    @Provides
    @Singleton
    fun provideRouter(cicerone: Cicerone<Router>): Router = cicerone.router

    @Named("string_to_date")
    @Singleton
    @Provides
    internal fun provideStringToDateConversionFormat(): DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)


    @Named("date_to_string_for_view")
    @Singleton
    @Provides
    internal fun provideDateToStringConversionFormatForView(locale: Locale): DateFormat {
        return if (locale.country == "RU") SimpleDateFormat("d MMMM, yyyy", locale)
        else SimpleDateFormat("MMMM d, yyyy", locale)
    }

    @Named("system_language")
    @Singleton
    @Provides
    internal fun provideSystemLanguage(locale: Locale): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) locale.toLanguageTag()
        else String.format("%s-%s", locale.language, locale.country)
    }

    @Singleton
    @Provides
    internal fun provideCurrentLocale(): Locale {
        val configuration = Resources.getSystem().configuration
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) getSystemLocale(configuration)
        else getSystemLocaleLegacy(configuration)
    }

    @Suppress("DEPRECATION")
    fun getSystemLocaleLegacy(config: Configuration): Locale = config.locale

    @TargetApi(Build.VERSION_CODES.N)
    fun getSystemLocale(config: Configuration): Locale = config.locales.get(0)


}